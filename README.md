[Click here to play now!](https://denhollander-thomas.gitlab.io/spotify-guesser)

# spotify-guesser

Guess the song in living room local multiplayer! The game is entirely done in the front-end, which means that the game can be played in any modern browser.

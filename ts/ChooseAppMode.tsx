import React from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardActionArea from "@material-ui/core/CardActionArea";
import Smartphone from "@material-ui/icons/Smartphone";
import Computer from "@material-ui/icons/Computer";

export interface Props {
  appModeChosen(appMode: "client" | "server"): void;
}

export default class ChooseAppMode extends React.Component<Props, {}> {
  public render(): JSX.Element {
    return (
      <React.Fragment>
        <Typography variant="h2" style={{ marginBottom: 16 }}>
          Choose your device:
        </Typography>
        <Typography variant="body1" style={{ marginBottom: 32 }}>
          This game is played with one host and multiple players. The host is
          visible for everyone, while the players each have their own device to
          answer on. You need to be on the same network to play, or the host has
          to forward the correct ports.
        </Typography>
        <Grid container spacing={16} direction={"row"}>
          <Grid item xs={12} md={6}>
            <Card
              raised={true}
              onClick={() => this.props.appModeChosen("client")}
            >
              <CardActionArea style={{ padding: 16 }}>
                <Smartphone color="action" style={{ width: 32, height: 32 }} />
                <Typography variant="h2" style={{ marginBottom: 16 }}>
                  Player
                </Typography>
                <Typography variant="body1">
                  Choose this option when you're a player and you want to
                  participate to an existing game.
                </Typography>
              </CardActionArea>
            </Card>
          </Grid>
          <Grid item xs={12} md={6}>
            <Card
              raised={true}
              onClick={() => this.props.appModeChosen("server")}
            >
              <CardActionArea style={{ padding: 16 }}>
                <Computer color="action" style={{ width: 32, height: 32 }} />
                <Typography variant="h2" style={{ marginBottom: 16 }}>
                  Host
                </Typography>
                <Typography variant="body1">
                  Choose this option to host a new game that others can join.
                </Typography>
              </CardActionArea>
            </Card>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}
